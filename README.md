# simple_filesystem
linux的文件系统驱动程序。

## How to use this?
1. Compile  
建议linux Kernel 4.14及以上，本案例以Kernel 5.10为例。从checkedout目录运行make即可。

2. Test  
````shell
sudo umount /root/testmodels/simple_filesystem/filesystem_test
sudo rmmod HUST_fs

dd bs=4096 count=100 if=/dev/zero of=image
./mkfs image
insmod HUST_fs.ko
mount -o loop -t HUST_fs image /root/testmodels/simple_filesystem/filesystem_test
dmesg
sudo chmod 777 filesystem_test -R
````
## Disk layout
Dummy block | Super block | bmap | imap |inode table | data block0 | data block1 | ... ...  

在mkfs.c上可以清楚地看到它们的结构。
