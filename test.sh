mkdir test_dir
sudo umount /root/testmodels/simple_filesystem/test_dir
sudo rmmod HUST_fs

dd bs=4096 count=100 if=/dev/zero of=image
./mkfs image
insmod HUST_fs.ko
mount -o loop -t HUST_fs image /root/testmodels/simple_filesystem/test_dir
dmesg
sudo chmod 777 test_dir -R